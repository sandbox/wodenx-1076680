DISCOUNT COUPON TRACKING
========================

This sub-module provides advanced tracking and management capabilities for use with the Ubercart Discount Coupons (uc_coupon) module.  The following functionality is provided:

1. The ability to assign coupons to an affiliate or salesman.
2. The ability for an administrator or coupon owner to control coupon distribution.
3. Drupal "Services" support for coupons, allowing external websites to view and distribute a coupon.
4. Views integration allowing tracking of coupon usage.

1. Coupon Assignment
--------------------
This simply allows anyone with "Manage Store Coupons" permission to assign a coupon to an affiliate or salesman.  The mechanism is exactly the same as that used by the "uc_coupon_purchase" sub-module.  You create an inactive coupon with the desired characteristics.  Then, when you assign that coupon, a new coupon with the same characteristics is created and given to the specified user.

2. Coupon Distribution
----------------------
The distribution tracking is designed with two aims in mind: (1) to prevent coupons from being used until they have been distributed, and (2) to prevent coupons from being distributed more often than they can be used.

The mechanism centers around the notion of "issuing" a coupon - that is, activating it for one or more uses.  This has a slightly different meaning depending on whether the coupon is a multiple-code (bulk) coupon or a single-code coupon.  For bulk coupons, each issue consists of one or more CODES.  For single-code coupons, each issue consists of one or more USES.  In either case, only the number of codes or uses issued will be considered active, and you can only issue as many codes or uses as are specified in the coupon definition.  The assumption is that a multiple-code coupon is designed to have each code distributed to a single customer, whlie a single-code coupon with multiple uses is designed to have the same code distributed to many customers.

For example, say you have a single-code coupon with 20 uses.  If you issue that coupon 10 times, then the code will only be valid at checkout for 10 uses.  You will also have a record of how many times you've issued the coupon, and only be able to issue it 10 times more.  On the other hand, if you have a bulk coupon with 20 codes, each of those codes will only be active once it has been issued, and you will only be able to issue each code once.

Additional features include the ability to list issues, print them, recall them (but do this with care - and only if you have not actually distributed the coupon code), and, if the "MimeMail" module is enabled,  e-mail a themed version of the coupons to the recipient.

Advanced tracking is enabled on a per-coupon basis via the coupon add/edit form.  In addition, only those users who have the "track own coupons" permission will have access to this feature.  This means that store administrators must be careful not to assign (or sell) coupons with advanced tracking enabled to users who do not have the permission to manage them, becuaswe the owner will not then be able to issue the coupons, so they will never be active.

Users without "track own coupons" permission will have the default "My Coupons" listing provided by "uc_coupon_purchase."


3. Services
-----------
The module exposes two methods: "uc_coupon.get" (which simply returns the coupon object), and "uc_coupon.issue" (which allows an affiliate site to distribute codes automatically).  Both are available only to users with "access coupon services" permission - if you do  not wish to grant this permission to anonymous users, you must also enable the "system_service" and "user_service" modules to allow the remote client to log in.  It is also recommended that you enable API key authentication for additinal security.  There is a PHP client script included with the module, which can be invoked on a non-Drupal website.

4. Coupon Usage Tracking
------------------------
This module provides views fields and relationships for tracking coupon usage, as well as several default views for displaying this information.  This feature depends on the "uc_views" module.  You can use the Views UI to browse the fields that are defined.  You can also specify your own views to replace the defaults via the module settings form.  This makes it very easy to customize the information that you make available to your affiliates.