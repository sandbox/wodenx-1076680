<?php
// $Id$
/**
 * @file
 * Views handler for a list of registration codes based on a coupon
 */

/**
 * Generate a list of the codes associated with this coupon
 */
class uc_coupon_tracking_handler_field_codes extends uc_coupon_handler_field_codes {

  /*
   * Append usage information to the code as themed by the parent
   */
  function render_item($count, $item) {
    $r = parent::render_item($count, $item);
    if ($this->options['show_usage']) {
      $n = (int) $item['coupon']->usage['codes'][$item['coupon']->code];
      $m = $item['coupon']->max_uses > 0 ? $item['coupon']->max_uses : '*';
      $r .=  " ($n/$m)";
    }
    return $r;
  }


  /**
   * Define option default values.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['show_usage'] = array('default' => FALSE);
    return $options;
  }

  /**
   * Provide options to limit number of codes, and to limit to coupons which still have uses remaining (or which don't)
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['scope']['#options']['issued'] = t('Issued codes');
    $form['scope']['#options']['unissued'] = t('Unissued codes');
    $form['show_usage'] = array(
      '#title' => t('Append usage information to each code'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['show_usage'],
    );
  }

  /**
   * Override parent include_coupon if this coupon is being tracked
   * 	- exclude unissued coupons if scope='avail'
   *  - set max_uses for bulk coupons to the number of issues
   *  - exclude or include coupons wthout calling parent if scope is 'issued' or 'unissued'
   */
  function include_coupon(&$coupon) {
    if ($coupon->data['track_dist']) {
      // A non bulk coupon can only be used as many times as it has been issued, so alter $coupon.
      if (!$coupon->bulk) {
        $n = uc_coupon_tracking_sum_issues($coupon->usage['issues']);
        $coupon->max_uses = $n < 0 ? 0 : $n;
      }
      // Check the scope.
      $issued = uc_coupon_tracking_is_issued($coupon);
      if (($this->options['scope'] == 'avail') && !$issued) {
        return FALSE;
      }
      elseif ($this->options['scope'] == 'issued') {
        return $issued;
      }
      elseif ($this->options['scope'] == 'unissued') {
        return !$issued;
      }
    }
    // If this is not a trackable coupon, then 'issued' and 'unissued' are equivalent to 'all'.
    elseif ($this->options['scope']=='issued' || $this->options['scope']=='unissued') {
      return TRUE;
    }

    return parent::include_coupon($coupon);
  }


}