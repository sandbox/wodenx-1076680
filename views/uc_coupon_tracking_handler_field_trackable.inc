<?php
// $Id$

/**
 * @file
 * Views handler: Filter coupons based on whether or not they are registration codes
 */

/**
 * Field handler for whether a coupon is trackable
 */
class uc_coupon_tracking_handler_field_trackable extends views_handler_field_boolean {
  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
    $this->field_alias = $this->table_alias .'_'. $this->field;
  }

  function render($values) {
    $data = unserialize($values->{$this->aliases['data']});
    $values->{$this->field_alias} = $data['track_dist'];
    return parent::render($values);
  }
}

