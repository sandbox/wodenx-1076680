<?php
// $Id$

/**
 * @file
 * Coupon order total field handler
 */
class uc_coupon_tracking_handler_field_issues extends views_handler_field_numeric {
  function query() {
    $this->ensure_my_table();
    $ucti = $this->query->ensure_table('uc_coupon_tracking_issues');

    $this->field_alias = $this->query->add_field(NULL, "SUM($ucti.number)",
      $this->table_alias .'_'. $this->field
    );
    $this->aliases['min'] = $this->query->add_field(NULL, "MIN($ucti.number)",
      $this->table_alias .'_' . 'min_issues'
    );

    $this->add_additional_fields();
  }

  function pre_render($values) {
    foreach ($values as $value) {
      // if not bulk coupon, then check for an unlimited issue
      if (!$value->{$this->aliases['bulk']} && $value->{$this->aliases['min']} <= 0) {
        $value->{$this->field_alias} = -1;
      }
    }
  }
}

