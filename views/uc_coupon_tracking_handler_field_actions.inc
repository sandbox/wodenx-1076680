<?php
// $Id$

/**
 * @file
 * Views handler: Coupon actions field.
 */

/**
 * Return a coupon value as price or percentage.
 */
class uc_coupon_tracking_handler_field_actions extends views_handler_field {
  function render($values) {
    return theme('uc_coupon_tracking_issue_actions', $values->{$this->field_alias}, $values->{$this->aliases['cid']});
  }
}

