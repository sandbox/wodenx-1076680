<?php
// $Id$

/**
 * @file
 * Views handler for a list of coupon codes associated with a particular issue
 */

/**
 * Generate a list of the codes associated with this issue
 * Special processing to display the #uses/max_uses for each non-bulk issue based on the size of that issue
 */
class uc_coupon_tracking_handler_field_issue_codes extends uc_coupon_handler_field_codes {
  function render_item($count, $item) {
    $r = parent::render_item($count, $item);
    $n = (int) $item['coupon']->usage['codes'][$item['coupon']->code];
    $m = $item['coupon']->max_uses > 0 ? $item['coupon']->max_uses : '*';
    return $r .  " ($n/$m)";
  }

  /*
   * Override parent to exclude unissued coupons if scope="avail"
   */
  function include_coupon($coupon) {
    if ($this->options['scope'] == 'avail' && !count($this->usage['issues'])) {
      return FALSE;
    }
    else {
      return parent::include_coupon($coupon);
    }
  }

  function pre_render($values) {
    $this->items = array();
    foreach ($values as $value) {
      // make sure this row has an issue
      if (!($ciid = $value->{$this->field_alias})) {
        continue;
      }

      // Load  the coupon for this issue, if we haven't already, and allocate usage.
      $cid = $value->{$this->aliases['cid']};
      if (!isset($coupons[$cid])) {
        $coupons[$cid] = uc_coupon_load($cid);
        $coupons[$cid]->usage = uc_coupon_count_usage($cid);
        if (!$coupons[$cid]->bulk && count($coupons[$cid]->usage['issues'])) {
          $uses = $coupons[$cid]->usage['codes'][$coupons[$cid]->code];
          uc_coupon_tracking_allocate_usage($uses, $coupons[$cid]->usage['issues']);
        }
      }

      // Initialize vars to generate codes
      $number = $value->{$this->aliases['number']};
      $start  = $value->{$this->aliases['start']};
      $coupon = $coupons[$cid];

      // Find the maximum number of codes to display.
      $limit = $coupon->bulk ? $number : 1;
      if ($this->options['max_num'] && $this->options['max_num'] < $limit) {
        $limit = $this->options['max_num'];
      }

      // List the codes
      for ($i = 0; $i < $limit; $i++) {
        $icoupon = clone$coupon;
        if ($coupon->bulk) {
          $icoupon->code = uc_coupon_get_bulk_code($coupon, $i + $start);
        }
        // for non bulk coupons, set the number of uses/max_uses as calculated above
        else {
          $icoupon->max_uses = $number < 0 ? 0 : $number;
          $icoupon->usage['codes'][$icoupon->code] = $icoupon->usage['issues'][$ciid]->uses;
        }
        if ($this->include_coupon($icoupon)) {
          $this->items[$ciid][] = array('coupon' => $icoupon);
        }
      }
    }
  }
}

