<?php
// $Id$

/**
 * @file
 * Views 2 hooks and callback registries.
 */

/**
 * Implementation of hook_views_data_alter().
 */
function uc_coupon_tracking_views_data_alter(&$data) {
  // replace codes handler with our own, which uses #issues to determine availability for tracked coupons
  $data['uc_coupons']['bulk_codes']['field']['handler'] = 'uc_coupon_tracking_handler_field_codes';

  $data['uc_coupons']['track_dist'] = array(
    'title' => t('Tracking Enabled'),
    'help' => t('Is distribution tracking enabled for this coupon.'),
    'field' => array(
      'handler' => 'uc_coupon_tracking_handler_field_trackable',
      'additional fields' => array(
        'data' => 'data',
      ),
      'output formats' => array(
         'asterisk' => array('*', ''),
      )
    ),
    'filter' => array(
      'handler' => 'uc_coupon_tracking_handler_filter_trackable',
      'label' => t('Tracking Enabled'),
    ),
  );

  $data['uc_coupons']['issues'] = array(
    'title' => t('Issued uses'),
    'help' => t('The total number of uses that have been issued for all codes associated with this coupon'),
    'field' => array(
      'handler' => 'uc_coupon_tracking_handler_field_issues',
      'additional fields' => array(
        'bulk' => 'bulk',
        'cid' => 'cid',
        'max_uses' => 'max_uses',
      ),

    ),
  );

  /*******************************************************
   * BEGINNING OF FIELDS WHICH BELONG IN UC_COUPON
   */
  $data['uc_coupons']['all_orders_count'] = array(
     'title' => t('All orders count'),
    'help' => t('The number of orders to which this coupon was applied.'),
    'field' => array(
      'handler' => 'uc_coupon_handler_field_all_orders_count',
    ),
  );
  $data['uc_coupons']['all_orders_count_old'] = array(
     'title' => t('All orders count old'),
    'help' => t('The number of orders to which this coupon was applied.'),
    'field' => array(
      'handler' => 'uc_coupon_handler_field_all_orders_count_old',
    ),
  );


  // The following fields/joins depend on the uc_orders table as defined in uc_views module
  if (module_exists('uc_views')) {
    $data['uc_coupons']['table']['join']['uc_orders'] = array(
      'left_table' => 'uc_coupons_orders',
      'left_field' => 'oid',
      'field' => 'order_id',
    );

    $data['uc_coupons']['all_orders_total'] = array(
      'title' => t('All orders total'),
      'help' => t('The total bottom-line for all orders to which this coupon was applied.'),
      'field' => array(
        'handler' => 'uc_coupon_handler_field_all_orders_total',
        'float' => TRUE,
      ),
    );

    $data['uc_coupons']['all_orders_value'] = array(
      'title' => t('All orders total value'),
      'help' => t('The total value of this coupon for all orders to which it was applied.'),
      'field' => array(
        'handler' => 'uc_coupon_handler_field_all_orders_value',
        'float' => TRUE,
      ),
    );

    $data['uc_coupons']['all_orders_gross'] = array(
      'title' => t('All orders gross total'),
      'help' => t('The pre-discount total for all orders to which this coupon was applied.'),
      'field' => array(
        'handler' => 'uc_coupon_handler_field_all_orders_gross',
        'float' => TRUE,
      ),
    );
  }

  //
  // Table: uc_coupons_orders
  //
  $data['uc_coupons_orders']['table']['group'] = t('Coupon order');
  $data['uc_coupons_orders']['table']['join']['uc_coupons'] = array(
    'left_field' => 'cid',
    'field' => 'cid',
  );

  // Join to the uc_orders table if it exists
  if (module_exists('uc_views')) {
    $data['uc_coupons_orders']['table']['join']['uc_orders'] = array(
      'left_field' => 'order_id',
      'field' => 'oid',
    );

    $data['uc_orders']['table']['join']['uc_coupons'] = array(
      'left_table' => 'uc_coupons_orders',
      'left_field' => 'oid',
      'field' => 'order_id',
    );
  }

  $data['uc_coupons_orders']['code'] = array(
    'title' => t('Code'),
    'help' => t('The coupon code used for this order'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['uc_coupons_orders']['value'] = array(
    'title' => t('Value'),
    'help' => t('The value of the coupon used for this order'),
    'field' => array(
      'handler' => 'uc_views_handler_field_money_amount',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['uc_coupons_orders']['gross'] = array(
    'title' => t('Gross'),
    'help' => t('The pre-discount bottom-line for this order'),
    'field' => array(
      'handler' => 'uc_coupon_handler_field_gross',
      'float' => TRUE,
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'uc_coupon_handler_filter_gross'
    ),
  );

  $data['uc_coupons_orders']['oid'] = array(
    'title' => t('Order ID'),
    'help' => t('The order ID of this coupon order'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  /*
   * END OF FIELDS WHICH BELONG IN UC_COUPON
   ***********************************************/



}


/**
 * Implementation of hook_views_data().
 */
function uc_coupon_tracking_views_data() {
  $data = array();

  $data['uc_coupon_tracking_issues']['table']['group'] = t('Coupon tracking');
  $data['uc_coupon_tracking_issues']['table']['join']['uc_coupons'] = array(
    'left_field' => 'cid',
    'field' => 'cid',
  );


  $data['uc_coupon_tracking_issues']['date'] = array(
    'title' => t('Issue date'),
    'help' => t('The date of this issue'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );



  $data['uc_coupon_tracking_issues']['ciid'] = array(
    'title' => t('Issue ID'),
    'help' => t('The unique ID of this issue.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  $data['uc_coupon_tracking_issues']['number'] = array(
    'title' => t('Size'),
    'help' => t('The number of codes or uses in this issue.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );



  $data['uc_coupon_tracking_issues']['details'] = array(
    'title' => t('Issue Details'),
    'help' => t('The details of this issue'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),

  );

  $data['uc_coupon_tracking_issues']['recipient'] = array(
    'title' => t('Issue Recipient'),
    'help' => t('The e-mail address of the person receiving this issue.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),

  );


   $data['uc_coupon_tracking_issues']['actions'] = array(
    'title' => t('Issue Actions'),
    'help' => t('Actions which can be performed on this issue'),
    'field' => array(
      'field' => 'ciid',
      'handler' => 'uc_coupon_tracking_handler_field_actions',
      'additional fields' => array(
        'cid' => 'cid',
      ),
    ),
  );


  $data['uc_coupon_tracking_issues']['codes'] = array(
    'title' => t('Issued Codes'),
    'help' => t('All codes associated with this issue.'),
    'field' => array(
      'handler' => 'uc_coupon_tracking_handler_field_issue_codes',
      'field' => 'ciid',
      'additional fields' => array(
        'cid' => 'cid',
        'start' => 'start',
        'number' => 'number',
      ),
    ),
  );





  return $data;
}



/**
 * Implementation of hook_views_handlers().
 */
function uc_coupon_tracking_views_handlers() {
  $handlers =  array(
    'info' => array(
      'path' => drupal_get_path('module', 'uc_coupon_tracking') .'/views',
    ),
    'handlers' => array(
      'uc_coupon_tracking_handler_field_codes' => array(
        'parent' => 'uc_coupon_handler_field_codes',
      ),
      'uc_coupon_tracking_handler_field_issue_codes' => array(
        'parent' => 'uc_coupon_tracking_handler_field_codes',
      ),
      'uc_coupon_tracking_handler_field_details' => array(
        'parent' => 'views_handler_field',
      ),
      'uc_coupon_tracking_handler_field_actions' => array(
        'parent' => 'views_handler_field',
      ),
      'uc_coupon_tracking_handler_field_issues' => array(
        'parent' => 'views_handler_field_numeric'
      ),
      'uc_coupon_tracking_handler_filter_trackable' => array(
        'parent' => 'views_handler_filter_boolean_operator'
      ),
      'uc_coupon_tracking_handler_field_trackable' => array(
        'parent' => 'views_handler_field_boolean'
      ),

    ),
  );

  /**************************************************
   * Extra handlers that should move to UC_COUPON
   */
  $handlers['handlers']['uc_coupon_handler_field_all_orders_count'] = array(
        'parent' => 'views_handler_field_numeric',
  );
  $handlers['handlers']['uc_coupon_handler_field_all_orders_count_old'] = array(
        'parent' => 'views_handler_field_numeric',
  );

  if (module_exists('uc_views')) {
    $handlers['handlers']['uc_coupon_handler_field_all_orders_total'] = array(
      'parent' => 'uc_views_handler_field_money_amount',
    );
    $handlers['handlers']['uc_coupon_handler_field_all_orders_value'] = array(
      'parent' => 'uc_coupon_handler_field_all_orders_total',
    );
    $handlers['handlers']['uc_coupon_handler_field_all_orders_gross'] = array(
      'parent' => 'uc_coupon_handler_field_all_orders_total',
    );
    $handlers['handlers']['uc_coupon_handler_field_gross'] = array(
      'parent' => 'uc_views_handler_field_money_amount',
    );
    $handlers['handlers']['uc_coupon_handler_filter_gross'] = array(
      'parent' => 'views_handler_filter_float',
    );

    /*
     * END OF EXTRA HANDLERS
     *************************************************/
  }

  return $handlers;

}
