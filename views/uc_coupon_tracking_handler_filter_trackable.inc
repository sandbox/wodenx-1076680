<?php
// $Id$

/**
 * @file
 * Views handler: Filter coupons based on whether or not they are registration codes
 */

/**
 * Filter coupons based on whether they apply to the uc_regcode type
 */
class uc_coupon_tracking_handler_filter_trackable extends views_handler_filter_boolean_operator {
  function query() {
    $string = 'uc_regcode';
    $this->query->add_field('uc_coupons', 'data');
    //$regexp = ".*product_types.*{[^}]*(uc_regcode).*}";
    $regexp = ".*\"track_dist\";b:1.*";
    //$this->query->add_where($this->options['group'], "uc_coupons.data ". (empty($this->value) ? "NOT " : "") ."LIKE '%%%s%%'", $string);
    $this->query->add_where($this->options['group'], "uc_coupons.data ". (empty($this->value) ? "NOT " : "") ."REGEXP '%s'", $regexp);
  }
}

