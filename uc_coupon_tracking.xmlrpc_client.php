<?php
// $Id$

/*
 * @file
 * Example of a PHP client for the Drupal uc_coupon_tracking module services.
 *
 * This example uses the XML-RPC for PHP library from http://phpxmlrpc.sourceforge.net/
 */


// Replace this with the path to your installation of XML-RPC for PHP
require_once ($_SERVER['HOME'] ."/sites/_lib/xmlrpc-3.0.0.beta/lib/xmlrpc.inc");

/**
 * Turn xmlrpcval back into PHP-native, recursively
 * (this function copied from the Drupal "Services Manual" at http://drupal.org/node/816934)
 *
 * @param $x The xmlrpcval to convert
 */
function rpc_convert($x) {
  $keys = array_keys($x->me);
  $val = &$x->me[$keys[0]];
  switch ($x->mytype) {
    // Scalar

    case 1:
      return $val;
    // Array

    case 2:
      $ret = array();
      foreach ($val as $v) {
        // Don't preserve keys
        $ret[] = rpc_convert($v);
      }
      return $ret;
    // Struct

    case 3:
      $ret = array();
      foreach ($val as $k => $v) {
        // Preserve keys
        $ret[$k] = rpc_convert($v);
      }
      return $ret;
  }
}

/**
 * Function for generating a random string, used for
 * generating a token for the XML-RPC session.
 * (This function copied from the Drupal "Services Manual" at http://drupal.org/node/339845)
 *
 * @param $length The length (in characters) of the string to return
 */
function get_unique_code($length = "") {
  $code = md5(uniqid(rand(), TRUE));
  if ($length != "") {
    return substr($code, 0, $length);
  }
  else return $code;
}

/**
 * Invoke an xmp-rpc method, optionally using API Key Authentication and a session ID.
 *
 * @param $client the xmlrpc_client object to use for the connection
 * @param $method_name  the name of the method to invoke (e.g. "system.connect")
 * @param $method_args an array of xmlrpcval objects representing the arguments to the method
 * @param $creds an array of authentication credentials, containing one or more of the following keys:
 * 	 'sessid' => The sessid to use for authentication.  If absent, no session id will be supplied.
 *   'key' => The API key.  If absent, no authentication will be used.
 *   'domain' => The domain associated with the specified API key. Must be specified if 'key' is specified.
 *
 * @return an array containing the following keys:
 * 	 'method' => The name of the rpc method invoked.
 *   'errno' => The error number of the response (or 0 if no error).
 *   'errstr' => The error message (not set if no error).
 *   'result' => The structured data returned by the method call.
 */
function rpc_invoke($client, $method_name, $method_args = array(), $creds = NULL) {
  // set up authentication
  if ($creds['key']) {
    $nonce     = get_unique_code(10);
    $timestamp = (string) strtotime('now');
    $hash      = hash_hmac('sha256', "$timestamp;". $creds['domain'] .";$nonce;$method_name", $creds['key']);

    // authentication parameters
    $authentication_args = array(
      new xmlrpcval($hash, 'string'),
      new xmlrpcval($creds['domain'], 'string'),
      new xmlrpcval($timestamp, 'string'),
      new xmlrpcval($nonce, 'string'),
    );

    // session id
    if ($creds['sessid']) {
      $authentication_args[] = new xmlrpcval($creds['sessid'], 'string');
    }
  }
  else {
    $authentication_args = array();
  }

  // buld param string for xml-rpc call
  $params = array_merge($authentication_args, $method_args);

  // send message and get response
  $message = new xmlrpcmsg($method_name, $params);
  $resp = $client->send($message);

  // build return array
  if ($resp->value()) {
    return array(
      'method' => $method_name,
      'result' => rpc_convert($resp->value()),
      'errno' => 0,
    );
  }
  else {
    return array(
      'method' => $method_name,
      'errno' => $resp->errno,
      'errstr' => $resp->errstr,
    );
  }
}

/**
 * Invoke a uc_coupon_tracking services method via xml-rpc.
 *
 * @param $server The url of the server.
 * @param $op The method, must be one of:
 *   'get' Fetch a coupon object.
 *   'issue' Issue a coupon code
 * @param $args An associative array of arguments for the method.  These depend on the value of $op:
 *   'get':
 *   		'cid'=>The coupon id
 *   'issue':
 *   		'cid'=>The coupon id
 *   		'recipient'=>The recipient (optional)
 * @param $creds An array of authentication credentials, containing one or more of the following keys:
 *   'key' => The API key.  If absent, no authentication will be used.
 *   'domain' => The domain associated with the specified API key. Must be specified if 'key' is specified.
 * 	 'sessid' => TRUE to obtain a session id and use it in authentication.
 *   'user' => The Drupal username to log-in.  If absent, method will be invoked anonymously.
 *   'password' => The Drupal password of the specified user.
 *
 * @return an array containing the following keys:
 * 	 'method' => The name of the rpc method invoked.
 *   'errno' => The error number of the response (or 0 if no error).
 *   'errstr' => The error message (not set if no error).
 *   'result' => The structured data returned by the method call.  Depends on the value of $op.
 *   	  'get':
 *   				The uc_coupon object.
 *   		'issue':
 *   				'code' => The issued code.
 *   				'html' => An html representation of the issued coupon
 */
function uc_coupon_client($server, $op, $args = NULL, $creds = NULL) {
  // build method parameters based on the operation
  $params = array();
  switch ($op) {
    case 'get':
      $params[] = new xmlrpcval($args['cid'], 'int');
      break;

    case 'issue':
      $params[] = new xmlrpcval($args['cid'], 'int');
      if (isset($args['recipient'])) {
        $params[] = new xmlrpcval($args['recipient'], 'string');
      }
      break;

    default:
      return array(
        'errno' => 400,
        'errstr' => 'Unsupported operation.',
      );
  }


  // create the client
  $client = new xmlrpc_client('/services/xmlrpc', $server, 80);

  // get session id (and log in if user is specified)

  if ($creds['sessid']) {
    $r = rpc_invoke($client, 'system.connect');
    if ($r['errno']) {
      return $r;
    }
    else {
      $creds['sessid'] = $r['result']['sessid'];
    }

    // log in if a username is specified
    if ($creds['user']) {
      $args = array(
        new xmlrpcval($creds['user'], 'string'),
        new xmlrpcval($creds['password'], 'string'),
      );
      $r = rpc_invoke($client, 'user.login', $args, $creds);
      if ($r['errno']) {
        return $r;
      }
      else {
        $creds['sessid'] = $r['result']['sessid'];
      }
    }
  }


  // invoke the method
  $result = rpc_invoke($client, "uc_coupon.$op", $params, $creds);

  // log out the user

  if ($creds['sessid'] && $creds['user']) {
    rpc_invoke($client, 'user.logout', array(), $creds);
  }


  return $result;
}


/// FOR EXAMPLE
$creds = array(
  'sessid' => TRUE,
  'domain' => 'www.authorized-domain.com',
  'key' => 'xxxxxxxxxxxxxxxxxxxxxxxxxx',
  'user' => 'username',
  'password' => 'password',
);

$args = array(
  'cid' => 1,
  'recipient' => 'recipient',
);

print_r(uc_coupon_client('www.example.com', 'issue', $args, $creds));

